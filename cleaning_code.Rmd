---
title: "Pangea clean"
author: "Chao Tang"
date: "18 September 2018"
output: html_document
---

```{r}
setwd("C:/Users/chao/Pangea_model")

#reading data
tenant=read.csv("tenant Before_August.csv")
unit=read.csv("unit Before_August.csv")
property=read.csv("property Before_August.csv")

#merging property and unit features into tenant
property=property[c("Building","Building.Type","Unit.Custom.Count","Count.Vacant.Units",
                    "Building.Status")]

unit=unit[c("BUILDING","HEAT_PAID_BY","BUILDING_STATUS","UNIT","TENANT","UNITTYPE","TOTAL_AREA",
            "BEDROOMS","BATHROOMS","TARGET_RENT","CURRENT_RENT","UNIT_STATUS",
            "Public.Assistance.Program","Amenities","Marketing.Cluster")]

unit=merge(unit,property,by.x="BUILDING",by.y="Building")

tenant=merge(tenant,unit,by.x=c("Property","Unit"),by.y=c("BUILDING","UNIT"))


#defining month of eviction
tenant$eviction_month=ifelse(tenant$Eviction.Filed==1,
                              (as.Date("01/08/2018", format="%d/%m/%Y")-as.Date(tenant$Move.In.Date, format="%d/%m/%Y"))/30,"NA")

#define eviction within 6 months as new response variable.
tenant$eviction2=ifelse(tenant$eviction_month<=6 & tenant$Eviction.Filed==1,1,0)

tenant$Marketing.Cluster[is.na(tenant$Marketing.Cluster)]=0

#imputation for missing values
tenant$CURRENT_RENT[is.na(tenant$CURRENT_RENT)]=mean(tenant$CURRENT_RENT[is.na(tenant$CURRENT_RENT)==F])

#replace % sign
tenant$AMI.. <- lapply(tenant$AMI.., gsub, pattern='%', replacement='')
tenant$AMI..=as.numeric(tenant$AMI..)


tenant$Portion.of.Rent.Tenant.Responsible<- lapply(tenant$Portion.of.Rent.Tenant.Responsible, gsub, pattern='%', replacement='')

tenant$Portion.of.Rent.Tenant.Responsible=as.numeric(tenant$Portion.of.Rent.Tenant.Responsible)

tenant$FICO=as.numeric(tenant$FICO)

#indicator of this Program
tenant$Public.Assistance.Program=ifelse(tenant$Public.Assistance.Program=="None"|tenant$Public.Assistance.Program=="",0,1)

tenant$HEAT_PAID_BY[tenant$HEAT_PAID_BY=="TBD"]="Tenant"

tenant$Move.Out.Date=as.Date(tenant$Move.Out.Date,"%d/%m/%Y")
tenant$Move.In.Date=as.Date(tenant$Move.In.Date,"%d/%m/%Y")
tenant$Eviction.Filed.Date=as.Date(tenant$Eviction.Filed.Date,"%d/%m/%Y")


#chaning names
colnames(tenant)[which(names(tenant) == "AMI..")] <- "AMI"


tenant$Income.monthly[tenant$Income.monthly==1]=mean(tenant$Income.monthly[tenant$Income.monthly!=1])

tenant$Rent.to.Income.Ratio[tenant$Rent.to.Income.Ratio>1]=tenant$Unit.Rent[tenant$Rent.to.Income.Ratio>1]/tenant$Income.monthly[tenant$Rent.to.Income.Ratio>1]



#deleting those features will not be used for modelling

tenant[,c("Pangea.Final.Decision","Reason.for.Decline","Flag.1","Flag.2","Flag.3","App.Submitted.Date","Became.Resident","Past.Tenant","Total.Write.Offs","Evicted...Filed.on..then.Moved.Out.","TENANT","Building.Type","Building.Status","UNIT_STATUS")]=list(NULL)



#unittype transform

tenant$UNITTYPE=as.character(tenant$UNITTYPE)

for (i in c(1:dim(tenant)[1])){
  if (grepl("Apartment",  tenant$UNITTYPE[i])){
     tenant$UNITTYPE[i]="Apartment"
  }
  
  else if (grepl("Garden",  tenant$UNITTYPE[i])){
     tenant$UNITTYPE[i]="Garden"
  } 
  
  else if (grepl("Townhouse",  tenant$UNITTYPE[i])){
     tenant$UNITTYPE[i]="Townhome"
  }
  
  else{ tenant$UNITTYPE[i]="Other"}
}

 tenant$UNITTYPE=as.factor(tenant$UNITTYPE)
```

```{r}
write.csv(tenant,"clean.csv")
```



```{r}

```


